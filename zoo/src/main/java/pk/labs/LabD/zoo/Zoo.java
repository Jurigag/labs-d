package pk.labs.LabD.zoo;

import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.zoo.internal.ActionStub;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Zoo {

    private static Zoo instance;

	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	private Set<Animal> animals = new HashSet<>();

	private Set<ServiceReference> actions = new HashSet<>();

	private ComponentContext componentContext;


    public void addAnimal(Animal animal) {
		Set<Animal> oldAnimals = animals;
		animals = new HashSet<>();
		animals.addAll(oldAnimals);
		if (animals.add(animal))
			pcs.firePropertyChange("animals", oldAnimals, animals);
	}

	public void removeAnimal(Animal animal) {
		Set<Animal> oldAnimals = animals;
		animals = new HashSet<>();
		animals.addAll(oldAnimals);
		if (animals.remove(animal))
            pcs.firePropertyChange("animals", oldAnimals, animals);
	}

	public Set<Animal> getAnimals() {
		return Collections.unmodifiableSet(animals);
	}

	public int getAnimalsCount() {
		return animals.size();
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

    public Set<ActionStub> getActionsFor(Collection<Animal> animals) {
        boolean first = true;
        Set<ServiceReference> actions = new HashSet<>();
        for (Animal animal : animals) {
            if (first) {
                actions.addAll(getActionsFor(animal));
                first = false;
            } else
                actions.retainAll(getActionsFor(animal));
        }
        Set<ActionStub> stubs = new HashSet<>();
        for (ServiceReference ref : actions)
            stubs.add(new ActionStub(componentContext, "action", ref));
        return stubs;
    }

    /**
     * Wyszukuje czynności dostępne dla danego zwięrzęcia
     * na podstawie metadanych czynności.
     * @param animal zwierzę, dla którego są pobierane czynności
     * @return zbiór referencji na dozwolone czynności
     */
	public Set<ServiceReference> getActionsFor(Animal animal) {
		Set<ServiceReference> result = new HashSet<>();
		for(ServiceReference ref : actions)
			if(ref.getProperty("species")==null)
				result.add(ref);
			else {
				String[] s = ((String)ref.getProperty("species")).split(",");
				for (String s1 : s) {
					if(s1.equals(animal.getSpecies())) {
						result.add(ref);
					}
				}
		}
		return result;
	}

    /**
     * Dodaje czynność zwierzęcia
     * @param ref referencja na nową usługę czynności
     */
    public void addAction(ServiceReference ref) {
		actions.add(ref);
    }

    /**
     * Usuwa nieaktualną czynność zwierzęcia
     * @param ref referencja do usuwanej usługi czynności
     */
    void removeAction(ServiceReference ref) {
		actions.remove(ref);
        // Do implementacji!
    }

	public void activate(ComponentContext context)
	{
		this.componentContext=context;
	}

}
