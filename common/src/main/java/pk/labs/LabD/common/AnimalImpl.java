package pk.labs.LabD.common;

import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class AnimalImpl implements Animal{
    String species;
    String name;
    String status;
    Logger logger;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status=status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }


    void setLogger(Logger logger)
    {
        this.logger=logger;
    }

    void activate(ComponentContext context)
    {
        name = (String) context.getProperties().get("name");
        species = (String) context.getProperties().get("species");
        logger.log(this, "activate "+this.name);
        System.out.println("activate");
    }


    void deactivate(ComponentContext context)
    {
        name = (String) context.getProperties().get("name");
        species = (String) context.getProperties().get("species");
        logger.log(this, "deactivate "+this.name);
        System.out.println("deactivate");
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpecies(String species) {
        this.species = species;
    }
}
