package pk.labs.LabD.actions.internal;

import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.AnimalAction;

/**
 * Created by Wojtek on 2015-01-24.
 */
public class Wstan implements AnimalAction {
    private String name;
    private String[] species;

    public String getName()
    {
        return this.name;
    }
    @Override
    public boolean execute(Animal animal) {
        if (animal != null)
        {
            animal.setStatus("stoi");
            return true;
        }
        return false;
    }

    public String name(){
        return this.name;
    }

    void activate(ComponentContext context){
        name=(String) context.getProperties().get("name");
        String species=(String) context.getProperties().get("species");
        if(species.contains(",")){
            String[] speciesList=species.split(",");
            this.species=speciesList;
        }
        else{
            this.species=new String[]{ species };
        }
    }
}